// My name is Abbhi Sekar, the date is September 8th, 2018. 
//This course is named CSE 002, and our section is 110.
//The purpose of this homework is to find the total cost of each item 
//Sales tax charged buying all of each kind of item (i.e. sales tax charged on belts)
//Total cost of purchases before tax
//Total sales tax
//Total paid for this transaction, including sales tax. 
//You must compute the cost of items you bought with the PA sales tax of 6%

public class Arithmetic {//Public class is named Cyclometer because that is what the function is 
    	// main method required for every Java program
   	public static void main(String[] args) {//The is the main method 
      
   //Number of pairs of pants
int numPants = 3;//This is the number of pants we have 
//Cost per pair of pants
double pantsPrice = 34.98;//THis is the price of pants defined as a double so there is 2 decimal points 

int numShirts = 2;//This is the number of sweatshirts we have 
double shirtPrice = 24.99;//This is the price per shirt 

int numBelts = 1;//The number of belts
double beltCost = 33.99;//cost per belt

double paSalesTax = 0.06;//This is the sales tax rate the PA
      //These are our assumptions 
      
double totalCostOfPants;   //total cost of pants
double totalCostOfShirts;//total cost of shirts 
double totalCostOfBelts;//total cost of belts 
double totalCostofPurchases;//total cost of purchases 
  
totalCostOfPants = numPants*pantsPrice; //total cost of pants in an equation 
totalCostOfShirts = numShirts*shirtPrice; //total cost of shirts in an equation 
totalCostOfBelts = numBelts*beltCost; //total cost of belts in an equation 
      
totalCostofPurchases = totalCostOfPants + totalCostOfShirts + totalCostOfBelts; //total cost of purchases before tax 
      
System.out.println("The total cost of pants before tax is " + totalCostOfPants + " dollars"); //the print statement for price of pants 
System.out.println("The total cost of shirts before tax is " + totalCostOfShirts + " dollars");//the print statement for price of shirts 
System.out.println("The total cost of belts before tax is " + totalCostOfBelts + " dollars");//the print statement for price of belts 
      
System.out.println("The total cost of Purchases before tax is " + totalCostofPurchases + " dollars");//the print statement for total purchases before tax
 
double SalesTaxPants; //defining sales tax on pants as a double variable 
double SalesTaxShirts; //defining sales tax on shirts as a double variable      
double SalesTaxBelts; //defining sales tax on belts as a double variable    
double TotalSalesTax; //defining total sales tax as a double variable
      
SalesTaxPants = paSalesTax*totalCostOfPants; //equation to calculate sales tax of pants
SalesTaxShirts = paSalesTax*totalCostOfShirts;//equation to calculate sales tax of shirts
SalesTaxBelts = paSalesTax*totalCostOfBelts;//equation to calculate sales tax of belts
TotalSalesTax = SalesTaxPants + SalesTaxShirts + SalesTaxBelts;//equation to calculate sales tax of all purchases together
    
String SalesTaxPantsString = String.format ("%.2f", SalesTaxPants); //Defining the double Sales Tax on Pants into a String in order to make it two decimal places 
String SalesTaxShirtsString = String.format ("%.2f", SalesTaxShirts);//Defining the double Sales Tax on Shirts into a String in order to make it two decimal places 
String SalesTaxBeltsString = String.format ("%.2f", SalesTaxBelts);//Defining the double Sales Tax on Belts into a String in order to make it two decimal places 
String TotalSalesTaxString = String.format ("%.2f", TotalSalesTax);//Defining the double Sales Tax on Purchases into a String in order to make it two decimal places 


System.out.println("The total sales tax on pants is " + SalesTaxPantsString + " dollars"); //statement that will print the sales tax of pants
System.out.println("The total sales tax on shirts is " + SalesTaxShirtsString + " dollars");//statement that will print the sales tax of shirts
System.out.println("The total sales tax on belts is " + SalesTaxBeltsString + " dollars");//statement that will print the sales tax of belts
System.out.println("The total sales tax on purchases is " + TotalSalesTaxString + " dollars");//statement that will print the sales tax of all purchases

      
double totalCostOfPantsTax;   //equation that defines total cost of pants with tax
double totalCostOfShirtsTax; //equation that defines total cost of shirts with tax
double totalCostOfBeltsTax; //equation that defines total cost of belts with tax
double totalCostOfPurchasesTax; //equation that defines total cost of purchases with tax
      
totalCostOfPantsTax = SalesTaxPants + totalCostOfPants; //equation to calculate cost of pants with tax 
totalCostOfShirtsTax = SalesTaxShirts + totalCostOfShirts; //equation to calculate cost of shirts with tax 
totalCostOfBeltsTax = SalesTaxBelts + totalCostOfBelts; //equation to calculate cost of belts with tax 
totalCostOfPurchasesTax = totalCostOfPantsTax + totalCostOfShirtsTax + totalCostOfBeltsTax; //equation to calculate cost of all purchases with tax 
      
String totalCostOfPantsTaxString = String.format ("%.2f", totalCostOfPantsTax); //Defining the double Sales Tax on Pants into a String in order to make it two decimal places 
String totalCostOfShirtsTaxString = String.format ("%.2f", totalCostOfShirtsTax);//Defining the double Sales Tax on Shirts into a String in order to make it two decimal places 
String totalCostOfBeltsTaxString = String.format ("%.2f", totalCostOfBeltsTax);//Defining the double Sales Tax on Belts into a String in order to make it two decimal places 
String totalCostOfPurchasesTaxString = String.format ("%.2f", totalCostOfPurchasesTax);//Defining the double Sales Tax on Purchases into a String in order to make it two decimal places 


System.out.println("The total cost of pants after tax is " + totalCostOfPantsTaxString + " dollars");//equation to print total cost of pants with tax 
System.out.println("The total cost of shirts after tax is " + totalCostOfShirtsTaxString + " dollars");//equation to print total cost of shirts with tax 
System.out.println("The total cost of belts after tax is " + totalCostOfBeltsTaxString + " dollars");//equation to print total cost of belts with tax 
System.out.println("The total cost of purchases after tax is " + totalCostOfPurchasesTaxString + " dollars");//equation to print total cost of purchases with tax 

       
      
    }
}

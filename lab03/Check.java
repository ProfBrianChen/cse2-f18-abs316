// My name is Abbhi Sekar, the date is September 7th, 2018. 
//This course is named CSE 002, and our section is 110 
//The Scenario is the user has gone out to dinner with friends. After they receive the bill, they decide to split the check evenly.
//The purpose of the program is to obtain from the user using the Scanner tool the original cost of the check
//The perecentage tip they wish to pay, and the number of ways the check will be split. 
//Then you must determine how much each person in the group needs to spend in order to pay the check.

import java.util.Scanner;//I imported the Scanner so manually input would be possible

public class Check{
    			// This is the class which I named Check 
   			public static void main(String[] args) { // This is the main method of the class 
          
Scanner myScanner = new Scanner(System.in); //in order to accept input I had to declare My Scanner - which is what I did here 

          System.out.print("Enter the original cost of the check in the form xx.xx: "); //This is a print statement to prompt the user to input a value for the check 
          double checkCost = myScanner.nextDouble();//This statement is so I can accept user input of the cost of the check
          
          System.out.print("Enter the percentage tip that you wish to pay as a whole number (in the form xx) : "); //This is a print statement that is prompting the user to say what tip they want to give
          double tipPercent = myScanner.nextDouble(); //This statement is so I can accept the user input of the tip percent 
          tipPercent /= 100; //We want to convert the percentage into a decimal 
          
          System.out.print("Enter the number of people who went out to dinner: "); //This print statement is prompting user to tell us how many people went ot dinner
          int numPeople = myScanner.nextInt(); // This statement is so I can accept the user input of the number of people
          

double totalCost; //this is defining the total cost of the payments
double costPerPerson; //This is defining how much it will cost per person 
int dollars;//this is defining dollars as an int varialble
int dimes; //this is defining dimes as an int varialble
int pennies; //this is defining pennies as an int varialble
totalCost = checkCost * (1 + tipPercent); //this is defining total cost to be equal to checking the cost times the percentage of tip 
costPerPerson = totalCost / numPeople; //This is the cost per person by taking total cost divided by the number of people
//get the whole amount, dropping decimal fraction
dollars=(int)costPerPerson; //This is defining dollars as cost per person
//get dimes amount, e.g., 
// (int)(6.73 * 10) % 10 -> 67 % 10 -> 7
//  where the % (mod) operator returns the remainder
//  after the division:   583%100 -> 83, 27%5 -> 2 
dimes=(int)(costPerPerson * 10) % 10;//this show the math to calculate dimes
pennies=(int)(costPerPerson * 100) % 10;//this shows the math to calculate pennies
System.out.println("Each person in the group owes $" + dollars + "." + dimes + pennies); //this shows how much each person owes in a print statement 

          
          


          
          
          

}  //end of main method   
  	} //end of class

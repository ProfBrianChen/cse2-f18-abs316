// My name is Abbhi Sekar, the date is September 15th, 2018. 
//This course is named CSE 002, and our section is 110.
//The purpose of this code is for the program to ask the user for doubles that represent the number of acres of land affected by hurricant percipitation
//We also need to ask the user how many inches of rain were dropped on average 
//The quanitity of rain sholud be in cubic miles 


import java.util.Scanner;//I imported the Scanner so manually input would be possible

public class Convert {//Public class is named Cyclometer because that is what the function is 
    	// main method required for every Java program
   	public static void main(String[] args) {//The is the main method 
      
   Scanner myScanner = new Scanner(System.in); //in order to accept input I had to declare My Scanner - which is what I did here 

          System.out.print("Enter the affected area in acres in the form xxxxx.xx: "); //This is a print statement to prompt the user to input a value for the check 
          double affectedArea = myScanner.nextDouble();//This statement is so I can accept user input of the cost of the check
      
        System.out.print("Enter the rainfall in the affected area in inches: "); //This is a print statement to prompt the user to input a value for the check 
          double averageRain = myScanner.nextDouble();//This statement is so I can accept user input of the cost of the check
      double totalInches; //this is define the total number of inches 
      double InchestoGallons;//This is defining thre variable inches to gallons 
      double totalGallons;//totalnumber of gallons defined
      double CubicMiles;//total cubic miles 
      InchestoGallons = 27154;//the conversion number
      totalInches = affectedArea * averageRain;//equation for total inches
      totalGallons = totalInches * InchestoGallons;//equation for total gallons
      CubicMiles = totalGallons * 9.08169e-13; //equations for cubic miles
      
         System.out.println(CubicMiles + " cubic miles"); //This is a print statement to prompt the user to input a value for rain in Cubic Miles 

      
      
      
    }
}
// My name is Abbhi Sekar, the date is September 15th, 2018. 
//This course is named CSE 002, and our section is 110.
//The purpose of this is to prompt the user for the dimensions of a pyramid and returns the volume inside the pyramid
//The measurements should be a type double

import java.util.Scanner;//I imported the Scanner so manually input would be possible

public class Pyramid {//Public class is named Cyclometer because that is what the function is 
    	// main method required for every Java program
   	public static void main(String[] args) {//The is the main method 
      
      Scanner myScanner = new Scanner(System.in); //in order to accept input I had to declare My Scanner - which is what I did here 

          System.out.print("The square side of the pyramid in feet is (input length): "); //This is a print statement to prompt the user to input a value for square side
          int squareSide = myScanner.nextInt();//This statement is so I can accept user input of the sqaure side of the pyramid 
          
          System.out.print("The height of the pyramid in feet is (input height): "); //This is a print statement that is prompting the user to say the height
          int height = myScanner.nextInt(); //This statement is so I can accept the user input of the height of the pyramid
      int AreaSquare; //This is the area of the bottom sqaure of the pyramid
      int VolumeRect; //This is the total volume of a rectangular prism before we divide by 3 for the volume of the pyramid
      int VolumePyramid;//This represents the final volume of the pyramid
      
     AreaSquare = squareSide * squareSide; //This is the equation to find the area of bottom sqaure - multiplied both sqaure sides
     VolumeRect = (AreaSquare) * (height); //This is the equation to find the volume of the rectangular prism before dividing by 3 for the pyramid volume
      VolumePyramid = VolumeRect/3;//This is the final step in order to find the volume of the pyramid
 
      
          System.out.print("The volume inside the pyramid is: " + VolumePyramid + ".");//This returns the volume of the pyramid through a print statement 
      
    }
}
      
       
       
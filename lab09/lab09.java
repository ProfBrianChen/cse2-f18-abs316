//My name is Abbhi Sekar, this is lab 09 and the date is November 16th
//The purpose of this lab is to learn how to print arrays through methods 

public class Lab09 {

	public static void main(String[] args) {
int[] array0 = {1,2,3,4,5,6,7,8,9,10};//this is the literal array of length 10
int [] array1 = copy(array0); //Pass array1 as copy of array0 
int [] array2 = copy(array0);//Pass array2 as copy of array0 

inverter(array0);
print(array0); //When this is printed it is printed reverse 
inverter2(array1);
print(array1); //This does not appear reversed because it is indicating to the new memory in inverter 2
int[] array3 = inverter2(array2);
print(array3); //This is reverse as a copy of array 2
	
	} //end of main method 
	
	public static int[] copy (int[]list) { // method that copies the original array 
		int[]list2 = new int[list.length];
		for (int k=0; k< list.length; k++) { //goes through all the values  
 			list2[k] = list[k];
		}
		return list2; //returning the new array 
	}
	
	//array to invert 
	public static void inverter (int[]list) {
		int k =0;
		for(k=0; k<list.length/2;k++) { //This goes from i to half the length 
			int temporary = list[k]; //This defines the array list as a temporary array
			list[k] = list[list.length-k-1]; 
			list[list.length-k-1] = temporary; //defining the variable is temporary 
		}
	}
	
	//The second inverter array 
	public static int[] inverter2(int[]list) {
		int[]copy = copy (list);
		int k =0;
		for(k=0;k<copy.length/2; k++) {
			int swap = copy[k];
			copy[k] = copy[copy.length-k-1]; 
			copy[copy.length-k-1] = swap; 
		}
		return copy; //returns the inverted version of the copied array created in the first array 	
	}
	
	public static void print(int[]list) {
		for (int l=0; l<list.length;l++) {
			System.out.println(list[l] + " "); //prints the list array 
		}
		System.out.println();
	} //closes print method 
	
		
} //closes class 

	
	
	
	


///////////////
///// CSE 02 WelcomeClass
///
public class WelcomeClass{//start for WelcomeClass code 
    
 	public static void main(String[] args) {//This is the main for Welcome Class
		
		System.out.println("  -----------");//This is the start of the program 
		System.out.println("  | WELCOME |");//This is where the Welcome is printed with the bars
		System.out.println("  ----------");//This is just a design feature that is asked
		System.out.println("  ^  ^  ^  ^  ^  ^");//This carrots are going under the Welcome sign 
		System.out.println(" / \\/ \\/ \\/ \\/ \\/ \\");//design feature - must have to brackets 
		System.out.println("<-E--J--K--O--O--O->");//Letters in all caps to display my point 
		System.out.println(" \\ /\\ /\\ /\\ /\\ /\\ /");//These brackets are design feature that is required for the assingment
		System.out.println("  v  v  v  v  v  v");	//This is the final line of the code that is needed to finish the design 
	}//end brackets
}//end brackets

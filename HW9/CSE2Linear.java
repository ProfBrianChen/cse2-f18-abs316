//My Name is Abbhi Sekar, the date is November 23rd, and this is homework 9 
//The goal of this homework is to have practice with searching arrays and single dimensional arrays
//Use arrays to input CSE final grades, search for a certain grade, and print how many times the grades is there

import java.util.Arrays; //importing arrays 
import java.util.Random; //importing random variables
import java.util.Scanner; //importing scanners

public class CSE2Linear {

	public static void main(String[] args) { //MAIN METHOD 
		Scanner scan = new Scanner(System.in); 
		
		System.out.println("Enter 15 ascending ints for final grades in CSE2: "); //Prompts user for 15 integers 
		
		String input = scan.nextLine();
		
		String[] CSgrade = input.split(" "); //splitting the string by spaces
		System.out.println(Arrays.toString(CSgrade)); //printing the array 
		int[] intArray = new int[CSgrade.length];  //the length of the array 
		for(int i = 0; i < CSgrade.length; i++) { //scoping through each element
		    intArray[i] = Integer.parseInt(CSgrade[i]); //parsing through the array '
		}		    
		int i=0;
		while (i < intArray.length) { //making sure it is while it is less than the length 
			if (intArray[i] < 0 || intArray[i] >100) { //ensuring that it is in the range
				System.out.println( "Error, integers must be between 1-100"); //error message
				break;
			}
			else {
				i++; //increasing the values 
			}
		}
		
		for (int j = 0; j < (CSgrade.length-1); j++) { //going through the length 
			if (intArray[j+1] <= intArray[j]) { //making sure it is ascending
				System.out.println("Error, the integers must be ascending"); //error message
				break;
			}
			
		}
		
		System.out.println("Enter a grade to search for: "); //prompting asking for the grade
		int m = scan.nextInt();
		System.out.println(binarySearch(m, intArray)); //binary search returning
		System.out.println("Scrambled: " + Arrays.toString(Scramble(intArray))); //returning the scramble

		System.out.println("Enter a grade to search for: "); //prompting asking for the grade
		int n = scan.nextInt();
		System.out.println(linearSearch(n, intArray)); //print the linear search array 

	
	} //end of main method 
	
	
//Method for Binary Search 
	
public static String binarySearch(int key, int[] intArray) { //arguments that you need to use in the method 
	int low =0;
	int high;
	int iterations = 0; //variable to count the number of iterations 
	high = intArray.length-1; //defining the top of the array is length minus 1
	
	while (high >= low ) { 	//while the top is great than the bottom 
		iterations+=1; //increase an iteration
		int mid = (low + high)/2; //define the middle as avg of top and bottom 
		if (key < intArray[mid]) { //if the number looking for is less than the middle
			high = mid - 1; //redefine as new top minus 1, because you never check that number twice
		}
		else if (key == intArray[mid]) {
			return "Your search was found, with " + iterations + " iteration(s)"; //if we find the number we're looking for print this 
		}
		else {
			low = mid + 1;
		}
	}
	return key + "was not found in the list with " + iterations + " iteration(s)";
	
} //closes the binary search method 


//Method for Scrambling Array 


public static int[] Scramble(int[] intArray) {
	
	int[] temporary = new int[15];// defining a temporary array to hold the value of cards 
	Random randNum = new Random(); //creating a random number generator

	for (int i=0; i<15; i++){ //i goes from 0 --> 14
		int k = randNum.nextInt(15); //this is defining k as a random integer under 101 
		temporary[k] = intArray[i]; //this is using the created array to hold the value of CSE grades
		intArray[i] = intArray[k]; //this is then taking that value and making it the new value
		intArray[k] = temporary[k]; //And this is transferring it to the temporary array 
	
}
	return intArray;
	
} //closes the scrambling method 

//Method for Linear Search 

public static String linearSearch(int key, int[] intArray) { //arguments that you need to use in the method 
	int iterations = 0; //variable to count the number of iterations 
	for (int i = 0; i < intArray.length; i++) 	{
		iterations+=1; //increase an iteration
		if (key == intArray[i]) 
			return "Your search was found, with " + iterations + " iteration(s)";
	}

	return key + "was not found in the list with " + iterations + " iteration(s)";

}

} //closes the class 

	
	

	
		
		
		

	
	
		















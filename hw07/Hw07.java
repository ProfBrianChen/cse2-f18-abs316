//My name is Abbhi Sekar, the date is October 27th, and I am in Professor Karr's Class
//The purpose of this assignment is to give us practice with writing methods 
//This assignment is supposed to output a inputed paragraph and menu and certain statistics about that paragraph

import java.util.Scanner;

public class Hw07 {

	public static void main(String[] args) { //main method
	
		System.out.println("You entered: " + sampleText()); //printing and returning the text entered in the string method
System.out.println(""); //formating
System.out.println(printMenu()); //printing the menu

if(printMenu() == 'c')	 { 
System.out.println(getNumOfNonWSCharacters()); //prints this method if 'c' is inputed 

}
if(printMenu() == 'w')	 {
	System.out.println(getNumOfWords()); //prints this method if 'w' is inputed 
}

if(printMenu() == 'f')	 {
	System.out.println(findText()); //prints this method if 'f' is inputed 
}
if(printMenu() == 'r')	 {
	System.out.println("Edited Text: " + replaceExclamation()); //prints this method if 'r' is inputed 
}

if(printMenu() == 's')	 {
	System.out.println("Edited Text: " + shortenSpace() ); //prints this method if 'r' is inputed 
}
if(printMenu() == 'q')	 {
	System.out.println("I quit"); //exit program 
}

	}
	
	
	public static String sampleText() {
		
		Scanner myScanner = new Scanner(System.in); //putting in scanner
	System.out.println("Enter a paragraph of your choosing"); //Asking for input for the string
    String output = myScanner.nextLine(); //value check as a integer
return output;  //returning the scanner as output
	

	}
	
	public static char printMenu() {
	
		//below I am printing the menu 
	System.out.println("MENU");
	System.out.println("c - Number of non-whitespace characters");
	System.out.println("w - Number of words");
	System.out.println("f - Find text");
	System.out.println("r - Replaces all !'s");
	System.out.println("s - Shorten spaces");
	System.out.println("q - Quit");
	
	//This scanner is used so the user can pick which option they want to know
	Scanner myScanner = new Scanner(System.in); //putting in scanner
System.out.println("");
	System.out.println("Choose an option:"); //Asking for input for the string
    char output = myScanner.next(".").charAt(0);; //value check as a integer
return output;  //returning the scanner as output	
	}
	
	//This method is being used to gather the amount of characters in the sample text inputed by the user earlier in the program
	public static int getNumOfNonWSCharacters() {
//int length = sampleText().length();
int length=sampleText().split(" ").length;
return length;
	}
	
	//This method is being used to gather the amount of words the user gets 
	public static int getNumOfWords() {
		int counter = 0;
		while (sampleText() == " ") { //Doing this by counting how many spaces, which should be equal to the amount of words + 1
		counter = counter +1;	
		}
		counter ++; //this accounts for not having a space at the end of the sentence 
		return counter; 
	}
	
	//This method is being used to find how many times a certain word is found in the sample text
	public static int findText() {
		Scanner myScanner = new Scanner(System.in); //putting in scanner
		System.out.println("Enter a word or phrase to be found: "); //Asking for input for the string
	    String output = myScanner.nextLine(); //value check as a integer
		int Numwords = 0; //initializing		
		// Numwords = how to you count the number of times a certain word is said 
		return Numwords;
		
	}
	
	//This method is used to replace all exclamation points within string to periods and output edited string
	public static String replaceExclamation() {
		String line;
		String ExclamationPoint="!";
		line=sampleText().replace(ExclamationPoint,".");
return line; 			
	}
	
	//This method is used to replace all two or spaces to one space for the edited text
	public static String shortenSpace() {
		String line;
		String TwoSpace="  ";
		line=sampleText().replace(TwoSpace," ");
return line; 	
		
	}
}
 	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	



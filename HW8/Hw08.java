//My name is Abbhi Sekar, the date is November 13th, and I am in Professor Karr's Class
//The purpose of this assignment is to give us practice using arrays
//This assignment is supposed to print out all the cards in a deck 
//And Shuffle the whole deck of cards, then print out the cards in the deck, all shuffled, then getting a hand of cards and print them out. 

//Write methods for shuffle(list), getHand(list,index, numCards), and printArray(list)

import java.util.Arrays; //importing arrays 
import java.util.Random; //importing random variables
import java.util.Scanner; //importing scanners

public class Hw08{  //start of the class 
	
public static void main (String[] args) {  //main method 
	
Scanner scan = new Scanner(System.in); 
 //suits club, heart, spade or diamond 


//Arrays formed by strings 
String[] suitNames={"C","H","S","D"};     
String[] rankNames={"2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q","K","A"}; 
String[] cards = new String[52];//0-51 
String[] hand = new String[5]; //0-4
//String[] temporary = new String[52];


int numCards = 5; //there are 5 cards 
int again = 1;  //repeat
int index = 51;//total 51 in hand


for (int i=0; i<52; i++){ //i goes from 0 --> 51
  cards[i]=rankNames[i%13]+suitNames[i/13];  // array cards =  remainder of 13 in rankNames and suit Names/13
  System.out.print(cards[i]+" "); //Print the array cards from 0-51
} 


System.out.println();
shuffle(cards); 


while(again == 1){ 
	System.out.println("How many cards would you like to have in your hand? ");
	numCards = scan.nextInt();
	hand = new String[numCards];
if (index < numCards - 1){ // if the index is less than the total cards minus 1 
    shuffle(cards); //then the cards will shuffle again 
    	index = 51; //this reverts the index back to 51 
  } // closes if statement
}//closes the while loop

index = index - numCards;
System.out.println("Hand ");
hand = getHand(cards,index,numCards); 


System.out.println(Arrays.toString(hand));
System.out.println("Enter a 1 if you want another hand drawn"); 
again = scan.nextInt(); 
  
} //closes the main method


//Shuffle Method 
public static String[] shuffle (String[]cards) {

	String[] temporary = new String[52];// defining a temporary array to hold the value of cards 
	Random randNum = new Random(); //creating a random number generator

	for (int i=0; i<52; i++){ //i goes from 0 --> 51
		int k = randNum.nextInt(52); //this is defining k as a random integer under 52 
		temporary[k] = cards[i]; //this is using the created array to hold the value of cards
		cards[i] = cards[k]; //this is then taking that value and making it the new value
		cards[k] = temporary[k]; //And this is transferring it to the temporary array 
}

System.out.println("Shuffled ");
System.out.println(Arrays.toString(cards));
return cards; 

} //This is the end of the shuffle method 


//GetHand Method 
public static String[] getHand (String[]cards, int index, int numCards) { //this method is taking the card array, index and number of cards is input 
	
	String [] hand = new String[numCards]; //This is creating a new array equaling an array with the number of cards as its input 
	for (int j =0; j < numCards; j++) { //while j goes from zero to the number of cards
		hand[numCards - j] = cards[index + numCards - j]; 
	}
	
	return hand; //return the array hand

} //End of GetHand method 



} //This is the end of the class 
// My name is Abbhi Sekar, the date is September 7th, 2018. 
//This course is named CSE 002, and our section is 110 
//The Purpose of this program is meant to be a biycle cyclometer (meant to measure speed, distance, etc.) 
//It Records two kinds of data, the time elapsed in seconds, and the number of rotations of the front wheel during that time.
// A needs to print number of minutes for each trip 
//B needs to print the number of trips 
//C prints the distance of each trip 
//D prints the distance between each trip 

public class Cyclometer {//Public class is named Cyclometer because that is what the function is 
    	// main method required for every Java program
   	public static void main(String[] args) {//The is the main method 
      
  int secsTrip1=480;  //This stores the number of seconds for trip 1 
  int secsTrip2=3220;  //This stores the number of seconds for tirp 2
	int countsTrip1=1561;  //This helps keep count of the number of rotations in trip 1
	int countsTrip2=9037; //This helps keep count of the number of rotations in trip 2 
      
  double wheelDiameter=27.0,  //This is a double variable defined as the diameter of the wheel in inches
  PI=3.14159, //This is the value of Pi that we have defined 
  feetPerMile=5280,  //This is the variable defined as 5280 feet, which is equal to one mile 
  inchesPerFoot=12,   //This is defining how many inches are in one foot 
  secondsPerMinute=60;  //This is define how many seconds is in one minute 
	double distanceTrip1, distanceTrip2,totalDistance;  //This is defining each of these variables as dobules so the program understands
      
  System.out.println("Trip 1 took "+ //This prints out Trip 1 took etc
  (secsTrip1/secondsPerMinute)+" minutes and had "+ //This physically prints the variables that were defined before divded by each other
  countsTrip1+" counts.");//This helps find how many trips are done per minutes 
	
  System.out.println("Trip 2 took "+ //This prints out how many trips were in number 2
  (secsTrip2/secondsPerMinute)+" minutes and had "+ //This is seeing how many trips took per minutes for #2 
  countsTrip2+" counts.");//And this is the counter here 
      
  distanceTrip1=countsTrip1*wheelDiameter*PI; //This is an equation solving for distance 1 for trip 2 - by multipling trip1count and PI and Wheel Diameter
  distanceTrip1/=inchesPerFoot*feetPerMile; // Gives distance in miles - This equation solives for the distrance of trip one but now in miles 
	distanceTrip2=countsTrip2*wheelDiameter*PI/inchesPerFoot/feetPerMile;//This is soliving for the variable distance trip2 per mile using counts, wheel diamters, and PI 
	totalDistance=distanceTrip1+distanceTrip2; //And this equation is adding the distance of both together to get the total trip 
      
  //Print out the output data.
  System.out.println("Trip 1 was "+distanceTrip1+" miles");//This is printing the final solution in one succint line for Trip 1 in miles 
	System.out.println("Trip 2 was "+distanceTrip2+" miles");//This is printing the final solution for trip 2 distance in miles 
	System.out.println("The total distance was "+totalDistance+" miles");//This is printing the total distance of both added together in miles 
      
      


  


      
      
      
	}  //end of main method   
} //end of class
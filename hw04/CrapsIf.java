// My name is Abbhi Sekar, the date is September 7th, 2018. 
//This course is named CSE 002, and our section is 110 
//The purpose of this program is to determine the slang terminology of the rolling of two dice roll and print it to the screen 
//This will use a random generator between 1-6. 

import java.util.Scanner;//I imported the Scanner so manually input would be possible

public class CrapsIf{
    			// This is the class which I named Check 
   			public static void main(String[] args) { // This is the main method of the class 
      
             Scanner myScanner = new Scanner(System.in); //in order to accept input I had to declare My Scanner - which is what I did here 

          System.out.println("Would you like to 'randomly' cast dice or would you like 'userprovided' dice?"); //This is a print statement to prompt the user decide on answer between the dice 
          String outcome = myScanner.nextLine();//This statement is so I can accept user input of the cost of the check
          
             int num1 = (int) (Math.random() * 6 + 1); //this is defining the random number that corerlates to the dice number 
             int num2 = (int) (Math.random() * 6 + 1); //this is defining the random number that corerlates to the dice number 
             int integer1 = 0;  //initialize
             int integer2 = 0;  //initialize
      
        
          if (outcome.equals("randomly")) {  //must be exact for if statement to work
            System.out.println("Dice 1 is " + num1); //print statement for dice 1 
            System.out.println("Dice 2 is " + num2);//print statement for dice 2
          }
          else if (outcome.equals("userprovided")){ //else if for userprovided section
            System.out.println("What is an integer between 1-6?"); //This is a print statement to prompt the user decide on answer between the dice 
            integer1 = myScanner.nextInt(); //myscanner for integer 1
            System.out.println("Dice one is " + integer1);//printing integer 1
            System.out.println("What is another integer between 1-6?"); //This is a print statement to prompt the user decide on answer between the dice 
            integer2 = myScanner.nextInt(); //myscanner for integer2
            System.out.println("Dice two is " + integer2); //printing integer 2
         }
       
          //randomly generated dice numbers 
          //-----------------------------------
          if (num1 == 1 && num2 == 1){ //if statement for random generated
          System.out.println("Snake Eyes");//print statement for defining num1 and num2
        }
          if (num1 == 1 && num2 == 2){//if statement for random generated
          System.out.println("Ace Deuce");//print statement for defining num1 and num2
        }
          if (num1 == 1 && num2 == 3){//if statement for random generated
          System.out.println("Easy Four");//print statement for defining num1 and num2
        }

           if (num1 == 1 && num2 == 4){//if statement for random generated
          System.out.println("Fever Five");//print statement for defining num1 and num2
        }
          if (num1 == 1 && num2 == 5){//if statement for random generated
          System.out.println("Easy Six");//print statement for defining num1 and num2
        }

           if (num1 == 1 && num2 == 6){//if statement for random generated
          System.out.println("Seven out");//print statement for defining num1 and num2
        }
          if (num1 == 2 && num2 == 2){//if statement for random generated
          System.out.println("Hard four");//print statement for defining num1 and num2
        }

           if (num1 == 2 && num2 == 3){//if statement for random generated
          System.out.println("Fever five");//print statement for defining num1 and num2
        }
          if (num1 == 2 && num2 == 4){//if statement for random generated
          System.out.println("Easy six");//print statement for defining num1 and num2
        }

           if (num1 == 2 && num2 == 5){//if statement for random generated
          System.out.println("Seven out");//print statement for defining num1 and num2
        }
          if (num1 == 2 && num2 == 6){//if statement for random generated
          System.out.println("Easy Eight");//print statement for defining num1 and num2
        }

           if (num1 == 3 && num2 == 3){//if statement for random generated
          System.out.println("Hard six");//print statement for defining num1 and num2
        }
          if (num1 == 3 && num2 == 4){//if statement for random generated
          System.out.println("Seven out");//print statement for defining num1 and num2
        }

           if (num1 == 3 && num2 == 5){//if statement for random generated
          System.out.println("Easy Eight");//print statement for defining num1 and num2
        }
          if (num1 == 3 && num2 == 6){//if statement for random generated
          System.out.println("Nine");//print statement for defining num1 and num2
        }

           if (num1 == 4 && num2 == 4){//if statement for random generated
          System.out.println("Hard Eight");//print statement for defining num1 and num2
        }
          if (num1 == 4 && num2 == 5){//if statement for random generated
          System.out.println("Nine");//print statement for defining num1 and num2
        }

           if (num1 == 4 && num2 == 6){//if statement for random generated
          System.out.println("Easy Ten");//print statement for defining num1 and num2
        }
             if (num1 == 5 && num2 == 5){//if statement for random generated
          System.out.println("Hard Ten");//print statement for defining num1 and num2
        }
          if (num1 == 5 && num2 == 6){//if statement for random generated
          System.out.println("Yo-leven");//print statement for defining num1 and num2
        }

           if (num1 == 6 && num2 == 6){//if statement for random generated
          System.out.println("Boxcars");//print statement for defining num1 and num2
        }
  //----------------------------Division of user provided and randon dice 
          if (integer1 == 1 && integer2 == 1){//if statement for userprovided generated
          System.out.println("Snake Eyes");//print statement for defining integer1 and integer2
        }
          if (integer1 == 1 && integer2 == 2){//if statement for userprovided generated
          System.out.println("Ace Deuce");//print statement for defining integer1 and integer2
        }
          if (integer1 == 1 && integer2 == 3){//if statement for userprovided generated
          System.out.println("Easy Four");//print statement for defining integer1 and integer2
        }

           if (integer1 == 1 && integer2 == 4){//if statement for userprovided generated
          System.out.println("Fever Five");//print statement for defining integer1 and integer2
        }
          if (integer1 == 1 && integer2 == 5){//if statement for userprovided generated
          System.out.println("Easy Six");//print statement for defining integer1 and integer2
        }

           if (integer1 == 1 && integer2 == 6){//if statement for userprovided generated
          System.out.println("Seven out");//print statement for defining integer1 and integer2
        }
          if (integer1 == 2 && integer2 == 2){//if statement for userprovided generated
          System.out.println("Hard four");//print statement for defining integer1 and integer2
        }

           if (integer1 == 2 && integer2 == 3){//if statement for userprovided generated
          System.out.println("Fever five");//print statement for defining integer1 and integer2
        }
          if (integer1 == 2 && integer2 == 4){//if statement for userprovided generated
          System.out.println("Easy six");//print statement for defining integer1 and integer2
        }

           if (integer1 == 2 && integer2 == 5){//if statement for userprovided generated
          System.out.println("Seven out");//print statement for defining integer1 and integer2
        }
          if (integer1 == 2 && integer2 == 6){//if statement for userprovided generated
          System.out.println("Easy Eight");//print statement for defining integer1 and integer2
        }

           if (integer1 == 3 && integer2 == 3){//if statement for userprovided generated
          System.out.println("Hard six");//print statement for defining integer1 and integer2
        }
          if (integer1 == 3 && integer2 == 4){//if statement for userprovided generated
          System.out.println("Seven out");//print statement for defining integer1 and integer2
        }

           if (integer1 == 3 && integer2 == 5){//if statement for userprovided generated
          System.out.println("Easy Eight");//print statement for defining integer1 and integer2
        }
          if (integer1 == 3 && integer2 == 6){//if statement for userprovided generated
          System.out.println("Nine");//print statement for defining integer1 and integer2
        }

           if (integer1 == 4 && integer2 == 4){//if statement for userprovided generated
          System.out.println("Hard Eight");//print statement for defining integer1 and integer2
        }
          if (integer1 == 4 && integer2 == 5){//if statement for userprovided generated
          System.out.println("Nine");//print statement for defining integer1 and integer2
        }

           if (integer1 == 4 && integer2 == 6){//if statement for userprovided generated
          System.out.println("Easy Ten");//print statement for defining integer1 and integer2
        }
             if (integer1 == 5 && integer2 == 5){//if statement for userprovided generated
          System.out.println("Hard Ten");//print statement for defining integer1 and integer2
        }
          if (integer1 == 5 && integer2 == 6){//if statement for userprovided generated
          System.out.println("Yo-leven");//print statement for defining integer1 and integer2
        }

           if (integer1 == 6 && integer2 == 6){//if statement for userprovided generated
          System.out.println("Boxcars");//print statement for defining integer1 and integer2
        }
       
       
          
          
          
          
          
          
          
          
          
          
          
          
          
          
          
         
          
         
          
          
        }
}